# ASC Comm522 RStudio Server Instructions

- [https://rstudio-comm522.asc.upenn.edu](https://rstudio-comm522.asc.upenn.edu)

## 1. SSH for initial login:

- Please ssh to the server in order to establish your server home directory. This is a necessary one time step!

- Instructions included below are for MacOS + ASCVPN *or* for your ASC Windows 10 virtual machine!


### From MacOS w/ ASC VPN connected: 

```
ssh <janus_username>@asc.upenn.edu@rstudio-comm522.asc.upenn.edu

# alternatively using IP instead of DNS
ssh <janus_username>@asc.upenn.edu@10.30.12.173
```

![](https://imghost.asc.upenn.edu/rstudio-images/macos_ssh_login.PNG)

- *OPTIONAL*: You can run `rstudio-server status` to confirm rstudio server is running!


### From your ASC Windows 10 Virtual Machine:

- On your Win10 VM, search for the application *PuTTY*, a common **shh client for Windows**...

- In the field *Host Name (or IP Address)* enter: `rstudio-comm522.asc.upenn.edu`
    - Alternatively, you can enter the IP address as `10.30.12.173`.

    ![](https://imghost.asc.upenn.edu/rstudio-images/win10_putty_ssh.PNG)

- Click **Open** to establish the ssh connection...
    - If you receive a security warning indicating host authenticity cannot be verified, this is expected given it's your first time logging in. Please select *yes* to continue. 
- Enter your JANUS credentials to authenticate on the server and create your home directory:
    - *login as:* `<janus_username>@asc.upenn.edu` & *your janus pw!*
![](https://imghost.asc.upenn.edu/rstudio-images/win10_putty_login.PNG)
    - *OPTIONAL*: You can run `rstudio-server status` to confirm rstudio server is running!


______

## 2. Accessing RStudio Server:

### Navigate in browser to rstudio-comm522.asc.upenn.edu 

- After you ssh & authenticated on the Linux host w/ your JANUS credentials, please navigate [https://rstudio-comm522.asc.upenn.edu](https://rstudio-comm522.asc.upenn.edu) and login with your JANUS credentials.
    - *REMEMBER*: The server is **only internally accessible on the ASC Network!** Thus you'll need the VPN connected to access from your local laptop, *or* you can simple access from inside your Win10 VM! 
    ![](https://imghost.asc.upenn.edu/rstudio-images/login_example.PNG)

- Click to **Sign in** to authenticate and reach your RStudio Server
______

## 3. Setting R Session Global Configs ...

### Configuring the Working Directory:
    
- On RStudio, in the top menu bar select **Tools > Global Configs ...** to open the *R Sessions Options* configuration menu. 
    ![](https://imghost.asc.upenn.edu/rstudio-images/tools_global_config.png)




- In the window that pops up, you need to set your *Default Working Directory for R Sessions*... Click on **Browse**.

    ![](https://imghost.asc.upenn.edu/rstudio-images/set-nfs-homedir.PNG)

- In the window that pops up, click the **three dots** in the upper right corner enter `/comm522/<janus_username>@asc.upenn.edu` (don't forget to replace with *YOUR* janus login!)

    ![](https://imghost.asc.upenn.edu/rstudio-images/three_dots_set_workdir.PNG)

- Click **OK** and then **Apply** to confirm your changes. Please restart your R Session for changes to take effect!
- You should now be ready to use the ASC Comm522 RStudio Server!

### *THIS FINAL STEP IS IMPORTANT BECAUSE...*
- Setting the working directory to `/comm522/<janus_username>@asc.upenn.edu` rather than the default local home directory `~/` is significant for various reasons:
    1. We don't want to fill up the local storage on the host as this will compromise system stability (no one in the class will be able to access the RStudio Server!)
    2. The dir `/comm522/` is an NFS mount and thus your data safely resides on the network / not the local host
    3. We have daily backups of `/comm522` in case you ever accidentally lose RStudio work!

